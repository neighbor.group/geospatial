# neighbor.group geospatial

This repository contains the necessary tools to setup the data used for neighbor.group maps.

## Dependencies

* [Python](https://www.python.org/)
* [pip](https://pip.pypa.io/en/stable/installing/)
* [GDAL](https://gdal.org/download.html)
	- [macOS GDAL binaries](https://www.kyngchaos.com/software/frameworks/)

## Setup

```
cd geospatial
./setup.sh
```

## Sourcing data

```
./source.sh
```

## Generating GeoJSON files

```
./data.sh
```
