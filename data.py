#!/usr/bin/env python

import json, os, sys
import us
import mapzen.whosonfirst.geojson
import mapzen.whosonfirst.utils

if len(sys.argv) < 2:
	print("Usage: data.py [source id]")
	exit(1)

script = os.path.realpath(sys.argv[0])
dir = os.path.dirname(script)
id = sys.argv[1]

encoder = mapzen.whosonfirst.geojson.encoder(precision=None)

print(f"Processing 'source/{id}/{id}.geojson'")
input = open(f"{dir}/source/{id}/{id}.geojson")

collection = json.load(input)
for feature in collection["features"]:
	props = feature["properties"]
	geoid = props["GEOID"]
	state = us.states.lookup(props["STATEFP"]).abbr
	state = str(state).lower()
	name = props["NAMELSAD"]
	feature["id"] = geoid
	feature["properties"] = {
		"geoid": geoid,
		"name": name,
		"state": state,
		"area_land": props["ALAND"],
		"area_water": props["AWATER"]
	}
	mapzen.whosonfirst.utils.ensure_bbox(feature)
	output = open(f"{dir}/data/{id}/{geoid}.geojson", 'w')
	encoder.encode_feature(feature, output)
