#!/bin/bash

set -e

script=`python -c 'import os, sys; print(os.path.realpath(sys.argv[1]))' $0`
dir=`dirname $script`

cd "$dir/source"
for id in * ; do
	if [ -d "$dir/source/$id" ] ; then
		mkdir -p "$dir/data/$id"
		../data.py "$id"
	fi
done

echo "Done"
