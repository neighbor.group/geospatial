#!/bin/bash

set -e

command -v pip >/dev/null 2>&1 || { echo >&2 "Error: 'pip' not installed."; exit 1; }

pip install -r requirements.txt
