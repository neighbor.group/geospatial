#!/bin/bash

set -e

command -v python >/dev/null 2>&1 || { echo >&2 "Error: 'python' not installed."; exit 1; }
command -v ogr2ogr >/dev/null 2>&1 || { echo >&2 "Error: 'ogr2ogr' not installed."; exit 1; }

script=`python -c 'import os, sys; print(os.path.realpath(sys.argv[1]))' $0`
dir=`dirname $script`

if [ "$1" == "" ] ; then
	echo "Which U.S. state are you sourcing data for? (e.g. NY)"
	read -p "> " state
else
	state="$1"
fi

id=`python -c "import us; print('tl_2019_' + us.states.lookup('$state').fips + '_bg')"`

mkdir -p "$dir/source"

if [ ! -d "$dir/source/$id" ] ; then
	url="https://www2.census.gov/geo/tiger/TIGER2019/BG/$id.zip"
	echo "Downloading $url"
	curl -o "$dir/source/$id.zip" "$url"
	echo "Unzipping 'source/$id.zip'"
	unzip -d "$dir/source/$id" "$dir/source/$id.zip"
fi

if [ ! -f "$dir/source/$id/$id.geojson" ] ; then
	echo "Processing shapefile -> 'source/$id/$id.geojson'"
	ogr2ogr -f GeoJSON -t_srs crs:84 "$dir/source/$id/$id.geojson" "$dir/source/$id/$id.shp"
else
	echo "'source/$id/$id.geojson' already exists"
fi

echo "Done"
